import { Component, OnInit } from '@angular/core'
import { ProductService } from './products/product.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title!: string

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.productService
      .getStoreData()
      .subscribe((data) => (this.title = data?.name ?? ''))
  }
}
