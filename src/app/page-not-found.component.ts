import { Component } from '@angular/core'

@Component({
  template: ` <div>{{ title }}</div> `,
})
export class PageNotFoundComponent {
  title = '404'
}
