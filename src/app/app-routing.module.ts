import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ProductListComponent } from './products/product-list/product-list.component'
import { PageNotFoundComponent } from './page-not-found.component'
import { ProductDetailComponent } from './products/product-detail/product-detail.component'
import { CreateProductFormComponent } from './products/create-product-form/create-product-form.component'

const routes: Routes = [
  { path: 'products', component: ProductListComponent },
  { path: 'products/:id', component: ProductDetailComponent },
  { path: 'create-product', component: CreateProductFormComponent },
  { path: '', redirectTo: '/products', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
