import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, catchError, of } from 'rxjs'
import { IStoreData } from './models/storeData'
import { IProduct } from './models/product'

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private url =
    'https://us-central1-test-b7665.cloudfunctions.net/api/stores/ijpxNJLM732vm8AeajMR'
  constructor(private http: HttpClient) {}

  getStoreData(): Observable<IStoreData | undefined> {
    return this.http
      .get<IStoreData>(this.url)
      .pipe(catchError(this.handleError('getStoreData', undefined)))
  }

  getProducts(): Observable<IProduct[]> {
    return this.http
      .get<IProduct[]>(`${this.url}/products`)
      .pipe(catchError(this.handleError('getProducts', [])))
  }

  getProductById(id: string): Observable<IProduct['data'] | undefined> {
    return this.http
      .get<IProduct['data']>(`${this.url}/products/${id}`)
      .pipe(catchError(this.handleError('getProductById', undefined)))
  }

  deleteProduct(id: string) {
    return this.http
      .delete(`${this.url}/products/${id}`)
      .pipe(catchError(this.handleError('deleteProduct')))
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: Error): Observable<T> => {
      console.error(operation, error)
      return of(result as T)
    }
  }
}
