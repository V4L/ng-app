export interface IProduct {
  id: string
  data: {
    title: string
    category: string
    price: string
    description: string
    employee: string
    reviews: string[]
  }
}
