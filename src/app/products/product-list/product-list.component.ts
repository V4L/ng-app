import { Component, OnInit } from '@angular/core'
import { ProductService } from '../product.service'
import { IProduct } from '../models/product'

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  products: IProduct[] = []
  loading = true

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.productService.getProducts().subscribe((products) => {
      this.products = products
      this.loading = false
    })
  }
}
