import { Component, OnInit } from '@angular/core'
import { IProduct } from '../models/product'
import { ProductService } from '../product.service'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {
  product: IProduct = {
    id: '',
    data: {
      title: '',
      category: '',
      price: '',
      description: '',
      employee: '',
      reviews: [],
    },
  }
  loading = true

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.productService.getProductById(id as string).subscribe((product) => {
      if (product) this.product.data = product
      else this.router.navigate(['/products'])
      this.loading = false
    })
  }

  deleteProduct(): void {
    console.log('deleting')
    const id = this.route.snapshot.paramMap.get('id')
    this.productService
      .deleteProduct(id as string)
      .subscribe(() => this.router.navigate(['/products']))
  }
}
