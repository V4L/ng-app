import { Component } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { IProduct } from '../models/product'

@Component({
  templateUrl: './create-product-form.component.html',
  styleUrls: ['./create-product-form.component.scss'],
})
export class CreateProductFormComponent {
  productForm!: FormGroup
  product!: IProduct

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.productForm = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(3)]],
      category: ['', Validators.required],
      price: ['', Validators.required],
      description: ['', Validators.required],
      employee: ['', Validators.required],
    })
  }

  save() {
    console.log(this.productForm)
    console.log('Saved: ' + JSON.stringify(this.productForm.value))
  }
}
